package com.example.almir.exerccio02;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;




public class Adapter extends BaseAdapter {


 public static    List<Usuario> usuario = new ArrayList<Usuario>();


    private Context context;

    public Adapter(Context context) {
        super();
        this.context = context;
    }

    @Override
    public int getCount() {
        return usuario.size();
    }

    @Override
    public Object getItem(int position) {
        return usuario.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String usuario1 =  usuario.get(position).getNome();

      View view = LayoutInflater.from(context).inflate(R.layout.activity_adapter, parent, false);

        TextView t = (TextView) view.findViewById(R.id.textView4);
        t.setText(usuario1);

        return view;
    }
}