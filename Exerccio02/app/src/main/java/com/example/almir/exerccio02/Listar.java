package com.example.almir.exerccio02;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class Listar extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        listView = (ListView) findViewById(R.id.lista);
        listView.setAdapter(new Adapter(this));
        listView.setOnItemClickListener(this);

    }
    public void onItemClick(AdapterView<?> parent, View view, int idx, long id) {


       Intent t= new Intent(this, informacoesdetalhadas.class);

       informacoesdetalhadas.id=idx;

       startActivity(t);
    }


}
