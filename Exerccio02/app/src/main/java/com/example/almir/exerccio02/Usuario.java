package com.example.almir.exerccio02;

public class Usuario {
    private String nome;
    private String cpf;
    private String nis;
    private String dependente;
    private String ndependente;

    public String getNdependente() {
        return ndependente;
    }

    public void setNdependente(String ndependente) {
        this.ndependente = ndependente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNis() {
        return nis;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public String getDependente() {
        return dependente;
    }

    public void setDependente(String dependente) {
        this.dependente = dependente;
    }
}
