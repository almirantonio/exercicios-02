package com.example.almir.exerccio02;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import static com.example.almir.exerccio02.Adapter.usuario;

public class Main2Activity extends AppCompatActivity {

    private static final String[] CPF = new String[]{
            "112.927.404-77", "322.927.504-77", "444.125.405-67"
    };

    // variavel que armazena o valor do Spinner
    Spinner listaop;
    // variavel que armazena o valor do  AutoCompleteTextView
    AutoCompleteTextView textView;
    // variavel que armazena o valor do  CheckBox
    String vCheckBox="Não  Possui o Numero do Nis";
    // variavel que armazena o valor do  RadioButton
    String vRadioButton="Não Possui Dependente";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ArrayAdapter<String> adapterAutoComplete = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, CPF);

        textView = (AutoCompleteTextView)
                findViewById(R.id.autoCompleteTextView2);
        textView.setAdapter(adapterAutoComplete);


        List<String> op = new ArrayList<String>();
        op.add("Número de Dependente");
        op.add("1");
        op.add("2");
        op.add("3");
        op.add("Mais de 3");
        listaop = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, op);
        listaop.setAdapter(adapter);

    }


    public void ok(View view) {

        //Pegando o nome do EditText a variavel que receberar o valor do campo será Getnome
        EditText nome = (EditText) findViewById(R.id.editText2);
        String Getnome = String.valueOf(nome.getText());

        //Pegando o número de dependente
        String numeroD = String.valueOf(listaop.getSelectedItemId());
        //Pegando o número do CPF

        String cpf = String.valueOf(textView.getText());
        //Atribuindo a um Array de String
        Usuario user=new Usuario();
       user.setNome(Getnome);
       user.setCpf(cpf);
       user.setNis(vCheckBox);
       user.setDependente(vRadioButton);
       user.setNdependente(numeroD);




        usuario.add(user);


        //Enviando pra outra tela
       final Intent it = new Intent(this,  Listar.class);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Atenção");
        builder.setMessage("Cadastro Realizado Sucesso");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(it);
                finish();
                return;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    //onCheckboxClicked verifica se o butão foi acionado
// ele colocar Já Possui o Numero do Nis na variavel se não  Não  Possui o Numero do Nis
    public void onCheckboxClicked(View view) {
        CheckBox c = (CheckBox) findViewById(R.id.checkBox3);
        boolean checked = (c.isChecked());

        if (checked == true) {
            vCheckBox = "Já Possui o Numero do Nis";
        }

    }

    //onRadioButton verifica se o butão foi acionado ele colocar
    // Possui Dependente na variavel se não  Não Possui Dependente
    public void onRadioButton(View view) {
        RadioButton f = (RadioButton) findViewById(R.id.radioButton5);
        boolean checked = (f.isChecked());

        if (checked == true) {
            vRadioButton = "Possui Dependente";
        }
    }

}
